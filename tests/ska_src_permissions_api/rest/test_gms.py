import pytest
from fastapi import HTTPException, Request
from mock import Mock
from starlette.datastructures import ImmutableMultiDict

from ska_src_permissions_api.client.iam import IamClient
from ska_src_permissions_api.rest.gms import Gms


def create_request(auth_token="Bearer some_token", query_params=None):
    """Helper function to create a mock request."""
    if query_params is None:
        query_params = ImmutableMultiDict()
    request_mock = Mock(spec=Request)
    request_mock.headers = {"Authorization": auth_token}
    request_mock.query_params = query_params
    return request_mock


class TestGms:
    @pytest.fixture(autouse=True)
    def iam_client(self):
        iam_client_mock = Mock(spec=IamClient)
        iam_client_mock.fetch_userinfo.return_value = {"sub": "user123"}
        iam_client_mock.fetch_user_by_id.return_value = {"sub": "user123",
                                                         "groups": [{"display": "group1"}, {"display": "group2"}]}
        return iam_client_mock

    @pytest.mark.asyncio
    async def test_get_groups_success(self, iam_client):
        gms = Gms(iam_client, 'etc/gms')
        request_mock = create_request(query_params=ImmutableMultiDict([('group', 'group1'), ('group', 'group2')]))

        result = await gms.get_groups(request=request_mock)
        assert result == "group1\ngroup2\n"

        iam_client.fetch_userinfo.assert_called_once_with(token="some_token")
        iam_client.fetch_user_by_id.assert_called_once_with(user_id="user123")

    @pytest.mark.asyncio
    async def test_get_groups_no_groups_in_user_profile(self, iam_client):
        gms = Gms(iam_client, config_relpath='etc/gms')
        request_mock = create_request(query_params=ImmutableMultiDict([('group', 'group1'), ('group', 'group2')]))

        iam_client.fetch_user_by_id.return_value = {"sub": "user123", "groups": []}

        result = await gms.get_groups(request=request_mock)
        assert result == ""

        iam_client.fetch_userinfo.assert_called_once_with(token="some_token")
        iam_client.fetch_user_by_id.assert_called_once_with(user_id="user123")

    @pytest.mark.asyncio
    async def test_get_groups_no_requested_groups(self, iam_client):
        gms = Gms(iam_client, config_relpath='etc/gms')
        request_mock = create_request(query_params=ImmutableMultiDict())

        result = await gms.get_groups(request=request_mock)
        assert result == "group1\ngroup2\n"

        iam_client.fetch_userinfo.assert_called_once_with(token="some_token")
        iam_client.fetch_user_by_id.assert_called_once_with(user_id="user123")

    @pytest.mark.asyncio
    async def test_get_groups_invalid_token(self, iam_client):
        gms = Gms(iam_client, config_relpath='etc/gms')
        request_mock = create_request(auth_token="Bearer invalid_token")

        iam_client.fetch_userinfo.side_effect = HTTPException(status_code=401, detail="Invalid token")

        with pytest.raises(HTTPException) as excinfo:
            await gms.get_groups(request=request_mock)

        assert excinfo.value.status_code == 401
        assert "Invalid token" in excinfo.value.detail

    def test_get_matching_groups(self, iam_client):
        gms = Gms(iam_client, config_relpath='etc/gms')
        user_profile = {"sub": "user123", "groups": [{"display": "group1"}, {"display": "group2"}]}
        requested_groups = ["group1"]

        result = gms.get_matching_groups(user_profile=user_profile, requested_groups=requested_groups)

        assert result == "group1\n"

    def test_get_matching_groups_no_match(self, iam_client):
        gms = Gms(iam_client, config_relpath='etc/gms')
        user_profile = {"sub": "user123", "groups": [{"display": "group1"}, {"display": "group2"}]}
        requested_groups = ["group3"]

        result = gms.get_matching_groups(user_profile=user_profile, requested_groups=requested_groups)

        assert result == ""

    def test_get_matching_groups_empty_group_list(self, iam_client):
        gms = Gms(iam_client, config_relpath='etc/gms')
        user_profile = {"sub": "user123", "groups": []}
        requested_groups = ["group1"]

        result = gms.get_matching_groups(user_profile=user_profile, requested_groups=requested_groups)

        assert result == ""

    def test_get_matching_groups_no_request_groups(self, iam_client):
        gms = Gms(iam_client, config_relpath='etc/gms')
        user_profile = {"sub": "user123", "groups": [{"display": "group1"}, {"display": "group2"}]}
        requested_groups = []

        result = gms.get_matching_groups(user_profile=user_profile, requested_groups=requested_groups)
        assert result == "group1\ngroup2\n"
