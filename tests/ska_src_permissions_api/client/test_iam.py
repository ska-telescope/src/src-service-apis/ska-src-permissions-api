import pytest
from mock import mock, Mock
from requests.exceptions import HTTPError

from ska_src_permissions_api.client.iam import IamClient


@pytest.mark.asyncio
class TestIamClient:
    @pytest.fixture(autouse=True)
    def iam_constants(self):
        iam_constants = Mock(name="IamConstants")
        iam_constants.iam_endpoint_token = "http://test.com/token"
        iam_constants.iam_endpoint_userinfo = "http://test.com/userinfo"
        iam_constants.iam_endpoint_scim = "http://test.com/scim"
        return iam_constants

    @mock.patch("httpx.AsyncClient.request")
    async def test_fetch_userinfo_success(self, mock_request, iam_constants):
        client = IamClient(iam_constants=iam_constants, client_id="test_id", client_secret="test_secret")
        mock_response = Mock(status_code=200, json=Mock(return_value={
            "sub": "mock_user",
            "name": "Mock User",
            "email": "mockuser@example.com",
            "organisation_name": "Mock Organization"
        }))

        mock_request.return_value = mock_response

        userinfo = await client.fetch_userinfo(token="test_token")
        expected_userinfo = {
            "sub": "mock_user",
            "name": "Mock User",
            "email": "mockuser@example.com",
            "organisation_name": "Mock Organization"
        }

        assert userinfo == expected_userinfo

        mock_request.assert_called_once_with(
            url="http://test.com/userinfo",
            method='GET',
            headers={
                "Authorization": "Bearer test_token",
                "Accept": "*/*",
            },
            data=None,
            timeout=10.0
        )

    @mock.patch("httpx.AsyncClient.request")
    async def test_fetch_userinfo_http_error(self, mock_get, iam_constants):
        client = IamClient(iam_constants=iam_constants, client_id="test_id", client_secret="test_secret")
        mock_get.side_effect = HTTPError("HTTP Error")

        with pytest.raises(HTTPError, match="HTTP Error"):
            await client.fetch_userinfo(token="test_token")

    @mock.patch("httpx.AsyncClient.request")
    async def test_fetch_user_by_id_success(self, mock_request, iam_constants):
        user_id = "user123"
        client = IamClient(iam_constants=iam_constants, client_id="test_id", client_secret="test_secret")

        token_response = {"access_token": "valid_token"}
        expected_response = {"sub": "user123", "name": "Mock User", "groups": [{"display": "group1"}]}

        mock_request.side_effect = [
            Mock(status_code=200, json=Mock(return_value=token_response)),
            Mock(status_code=200, json=Mock(return_value=expected_response))
        ]

        user = await client.fetch_user_by_id(user_id="user123")
        assert user == expected_response

        scim_url = "http://test.com/scim"
        fetch_url = f"{scim_url}/Users/{user_id}"

        assert mock_request.call_count == 2

    @mock.patch("httpx.AsyncClient.request")
    async def test_fetch_user_by_id_http_error(self, mock_request, iam_constants):
        user_id = "user123"
        mock_request.side_effect = HTTPError("HTTP Error")

        client = IamClient(iam_constants=iam_constants, client_id="test_id", client_secret="test_secret")

        with pytest.raises(HTTPError, match="HTTP Error"):
            await client.fetch_user_by_id(user_id=user_id)
