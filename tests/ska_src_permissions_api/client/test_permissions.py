from mock import Mock, mock

from ska_src_permissions_api.client.permissions import PermissionsClient


class TestPermissionsClient:
    client = PermissionsClient("localhost")

    @mock.patch('requests.Session.get', return_value=Mock(status_code=200))
    def test_health(self, mock_get):
        value = self.client.health()
        assert value.status_code == 200
        mock_get.assert_called_once_with("localhost/health")

    @mock.patch('requests.Session.get', return_value=Mock(status_code=200))
    def test_ping(self, mock_get):
        value = self.client.ping()
        assert value.status_code == 200
        mock_get.assert_called_once_with("localhost/ping")

    @mock.patch('requests.Session.post', return_value=Mock(status_code=200))
    def test_authorise_route_by_plugin(self, mock_post):
        value = self.client.authorise_route_by_plugin("service", "version", token="token")
        assert value.status_code == 200
        mock_post.assert_called_once_with(
            "localhost/authorise/plugin/service", params={'version': 'version', 'token': 'token'}, data='{}'
        )

    @mock.patch('requests.Session.post', return_value=Mock(status_code=200))
    def test_authorise_service_route(self, mock_post):
        mock_post.return_value = Mock(status_code=200)
        value = self.client.authorise_service_route("service", "version", "route", "method", token="token")
        assert value.status_code == 200
        mock_post.assert_called_once_with(
            'localhost/authorise/route/service',
            params={'version': 'version', 'route': 'route', 'method': 'method', 'token': 'token'}, data='{}'
        )

    @mock.patch('requests.Session.post', return_value=Mock(status_code=200))
    def test_authorise_service_exchange(self, mock_post):
        value = self.client.authorise_service_exchange("service", "version", token="token")
        assert value.status_code == 200
        mock_post.assert_called_once_with(
            'localhost/authorise/exchange/service', params={'version': 'version', 'token': 'token'}
        )
