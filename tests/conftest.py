import sys
from os import path

print("****************************** Adding project_path to sys PATH ******************************")
project_path = path.dirname(path.dirname(path.abspath(__file__)))
sys.path.append(f'{project_path}/src')
