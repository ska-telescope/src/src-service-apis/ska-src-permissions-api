FROM python:3.8-bullseye

USER root

RUN groupadd user
RUN adduser --system --no-create-home --disabled-password --shell /bin/bash user

COPY --chown=user . /opt/ska-src-permissions-api

RUN cd /opt/ska-src-permissions-api && python3 -m pip install -e .

WORKDIR /opt/ska-src-permissions-api

ENV DISABLE_AUTHENTICATION ''
ENV API_ROOT_PATH ''
ENV API_IAM_CLIENT_ID ''
ENV API_IAM_CLIENT_SECRET ''
ENV API_IAM_CLIENT_SCOPES ''
ENV API_IAM_CLIENT_AUDIENCE ''
ENV API_IAM_CLIENT_TIMEOUT_S ''
ENV IAM_CLIENT_CONF_URL ''
ENV PERMISSIONS_SERVICES_ROOT_GROUP_NAME ''
ENV PERMISSIONS_DATA_ROOT_GROUP_NAME ''
ENV PERMISSIONS_RELPATH ''
ENV PERMISSIONS_INCLUDE_POLICIES_FQN ''
ENV SCHEMAS_RELPATH ''
ENV VALIDATE_TOKENS_OFFLINE = ''
ENV IVOA_GMS_CONFIG_RELPATH = ''

ENTRYPOINT ["/bin/bash", "etc/docker/init.sh"]
