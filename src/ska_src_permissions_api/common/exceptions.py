import requests
import traceback
from functools import wraps

from fastapi import HTTPException, status


def handle_client_exceptions(func):
    """ Decorator to handle client exceptions. """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except requests.exceptions.HTTPError as e:
            status_code = e.response.status_code
            detail = f"HTTP error occurred: {e}, response: {e.response.text}"
            raise HTTPException(status_code=status_code, detail=detail)
        except HTTPException as e:
            raise e
        except CustomException as e:
            raise Exception(message=e.message)
        except CustomHTTPException as e:
            raise HTTPException(status_code=e.http_error_status, detail=e.message)
        except Exception as e:
            detail = "General error occurred: {}, traceback: {}".format(
                repr(e), ''.join(traceback.format_tb(e.__traceback__)))
            raise HTTPException(status_code=500, detail=detail)
    return wrapper


def handle_exceptions(func):
    """ Decorator to handle server exceptions. """
    @wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except requests.exceptions.HTTPError as e:
            status_code = e.response.status_code
            detail = f"HTTP error occurred: {e}, response: {e.response.text}"
            raise HTTPException(status_code=status_code, detail=detail)
        except HTTPException as e:
            raise e
        except CustomException as e:
            raise Exception(message=e.message)
        except CustomHTTPException as e:
            raise HTTPException(status_code=e.http_error_status, detail=e.message)
        except Exception as e:
            detail = "General error occurred: {}, traceback: {}".format(
                repr(e), ''.join(traceback.format_tb(e.__traceback__)))
            raise HTTPException(status_code=500, detail=detail)
    return wrapper


class CustomException(Exception):
    """ Class that all custom exceptions must inherit in order for exception to be caught by the
    handle_exceptions decorator.
    """
    pass


class IAMEndpointNotFoundInWellKnown(CustomException):
    def __init__(self, endpoint):
        self.message = "Error setting IAM {} endpoint, not found in .well_known".format(endpoint)
        super().__init__(self.message)


class PermissionPolicyMalformed(CustomException):
    def __init__(self, policy_relpath, reason):
        self.message = "The permissions policy at {} is malformed: {}".format(policy_relpath, reason)
        super().__init__(self.message)


class PermissionPolicySchemaMalformed(CustomException):
    def __init__(self, type, reason):
        self.message = "The permissions policy schema is malformed for type {}: {}".format(type, reason)
        super().__init__(self.message)


class PermissionPolicySchemaNotFoundForPolicyType(CustomException):
    def __init__(self, type):
        self.message = "The permissions policy schema could not be found for type {}".format(type)
        super().__init__(self.message)


class PermissionPolicySchemaValidationFailed(CustomException):
    def __init__(self, policy_relpath, reason):
        self.message = "The permissions policy at {} failed schema validation: {}".format(policy_relpath, reason)
        super().__init__(self.message)


class CustomHTTPException(Exception):
    """ Class that all custom HTTP exceptions must inherit in order for exception to be caught by
    the handle_exceptions decorator.
    """
    pass


class MissingPermissionKeyword(CustomHTTPException):
    def __init__(self, keyword):
        self.keyword = keyword
        self.message = "Missing keyword for permission: {}".format(keyword)
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class NoPermissionDefinitionForRoute(CustomHTTPException):
    def __init__(self, message="The permission definition for this route does not exist."):
        self.message = message
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class NoPermissionPolicyForService(CustomHTTPException):
    def __init__(self, message="The permission policy for this service does not exist."):
        self.message = message
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class NoPermissionPolicyForServiceVersion(CustomHTTPException):
    def __init__(self, message="The permission policy for this service version does not exist."):
        self.message = message
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class NoPermissionPolicyForType(CustomHTTPException):
    def __init__(self, message="No permission policies exist for this type."):
        self.message = message
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class NoPublicKeyForToken(CustomHTTPException):
    def __init__(self, message="No public key found to decode this token."):
        self.message = message
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class PermissionExpressionMalformed(CustomHTTPException):
    def __init__(self, expression):
        self.expression = expression
        self.message = "Permission expression for this route is malformed: {}".format(
            expression)
        self.http_error_status = status.HTTP_500_INTERNAL_SERVER_ERROR
        super().__init__(self.message)


class PermissionPluginNotFound(CustomHTTPException):
    def __init__(self, plugin_module_name):
        self.plugin_module_name = plugin_module_name
        self.message = "The permission plugin module '{}' could not be found.".format(plugin_module_name)
        self.http_error_status = status.HTTP_500_INTERNAL_SERVER_ERROR
        super().__init__(self.message)


class TokenExchangeError(CustomHTTPException):
    def __init__(self, reason):
        self.message = "Error during token exchange: {}".format(reason)
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)


class TokenInactive(CustomHTTPException):
    def __init__(self):
        self.message = "Token is inactive. This may be due to an invalid or expired token."
        self.http_error_status = status.HTTP_401_UNAUTHORIZED
        super().__init__(self.message)


class TokenInvalidAudience(CustomHTTPException):
    def __init__(self, presented, expected):
        self.message = "Token has an incorrect audience, got '{}' expected '{}'".format(
            presented, expected)
        self.http_error_status = status.HTTP_401_UNAUTHORIZED
        super().__init__(self.message)


class TokenInvalidIssuer(CustomHTTPException):
    def __init__(self, presented, expected):
        self.message = "Token has an incorrect issuer, got '{}' expected '{}'".format(
            presented, expected)
        self.http_error_status = status.HTTP_401_UNAUTHORIZED
        super().__init__(self.message)


class TokenMissing(CustomHTTPException):
    def __init__(self):
        self.message = "Token is missing from body."
        self.http_error_status = status.HTTP_400_BAD_REQUEST
        super().__init__(self.message)
