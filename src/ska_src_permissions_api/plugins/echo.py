from ska_src_permissions_api.plugins.plugin import PermissionPlugin


class EchoServicePermissionPlugin(PermissionPlugin):
    def __init__(self, services_root_group, data_root_group, groups, scopes, params):
        super().__init__(services_root_group, data_root_group, groups, scopes, params, add_common_required_groups=True)

    def has_permission(self):
        try:
            # Evaluate permission
            return self.evaluate_permission()
        except AssertionError as e:
            self.logger.info(e)
            return False
        except Exception as e:
            self.logger.warning(e)
            return False
