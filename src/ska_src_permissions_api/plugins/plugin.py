import abc
import logging


class PermissionPlugin(abc.ABC):
    def __init__(self, services_root_group, data_root_group, groups, scopes, params, add_common_required_groups=True):
        self.services_root_group = services_root_group
        self.data_root_group = data_root_group
        self.groups = groups
        self.scopes = scopes

        self.logger = logging.getLogger("uvicorn")              # Set logging to use uvicorn logger.

        # Assign service parameters and sanity check.
        #
        self.service = params.get('service', {})
        assert self.service.get('node') is not None, "No service node specified."
        assert self.service.get('type') is not None, "No service type specified"
        assert self.service.get('uuid') is not None, "No service uuid specified."
        if self.service.get('attributes') is None:
            self.logger.debug("No service attributes specified, setting to {}")
            self.service['attributes'] = {}
        if self.service.get('parameters') is None:
            self.logger.debug("No service request parameters specified, setting to {}")
            self.service['parameters'] = {}
        if self.service.get('body') is None:
            self.logger.debug("No service request parameters specified")
        if self.service.get('method') is None:
            self.logger.debug("No service request method specified")
        if self.service.get('path') is None:
            self.logger.debug("No service request path specified")

        # Required groups to be checked for membership.
        #
        # This is a list of lists, where the entries in each list form an AND clause, and the entries in each
        # sublist form an OR clause.
        #
        self.required_groups = []

        # Add common required groups, if requested.
        #
        if add_common_required_groups:
            self._add_common_required_groups()

    def _add_common_required_groups(self):
        """ Add required groups that are common across all plugins. """
        #FIXME: Add groups related to service?
        pass

    def evaluate_permission(self):
        for or_clause in self.required_groups:
            group_found = False
            for group in or_clause:
                if group in self.groups:
                    group_found = True
                    break
            if not group_found:
                return False
        return True

    def has_permission(self):
        raise NotImplementedError

