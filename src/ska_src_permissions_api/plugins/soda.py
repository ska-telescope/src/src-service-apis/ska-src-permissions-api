import re

from ska_src_permissions_api.plugins.plugin import PermissionPlugin


# Define fallback methods to decompose ivoid to namespace and name.
#
def decompose_ivoid_deterministic(ivoid):
    """ Assumes a deterministic backend RSE presenting an ID like ...?/namespace/aa/bb/name. """
    namespace_match = re.search("\\?([^\\/]+)", ivoid)
    name_match = re.search( ".*\\/(.*)", ivoid)
    if namespace_match and name_match:
        namespace = namespace_match.group(1)
        name = name_match.group(1)
    else:
        return False
    return namespace, name


# Create a list of these fallback methods to be called in order. The first one to return not False will be used.
#
fallback_ivoid_decomposition_functions = [
    decompose_ivoid_deterministic
]


class SODAServicePermissionPlugin(PermissionPlugin):
    """ A plugin for SODA services. """
    def __init__(self, services_root_group, data_root_group, groups, scopes, params):
        super().__init__(services_root_group, data_root_group, groups, scopes, params, add_common_required_groups=True)

        # Sanity check bespoke parameters for this service.
        #
        assert self.service.get('parameters', {}).get('ID') is not None, "No ID specified in service parameters."

    def _handle_get(self):
        """ Handle GET requests.

        A GET request expects the following body:

        {
          "service": {
            "node": site name from site-capabilities API
            "type": service type from site-capabilities API
            "uuid": service uuid from site-capabilities API
            "parameters": {
              "ID": the IVOID of the data identifier
            },
            "attributes": {
              "ivoid_to_namespace_regex": a regex for how to decompose an ivoid to a namespace
              "ivoid_to_name_regex": a regex for how to decompose an ivoid to a namespace
            }
          }
        }

        e.g.

        {
          "service": {
            "node": "SKAOSRC",
            "type": "soda_sync",
            "uuid": "05e18fb5-5f32-4c24-a399-0c50c77fb6d7",
            "parameters": {
              "ID": "ivo://auth.example.org/datasets/fits?testing/5b/f5/PTF10tce.fits"
            },
            "attributes": {
              "ivoid_to_namespace_regex": "\\?([^\\/]+)",
              "ivoid_to_name_regex": ".*\\/(.*)"
            }
          }
        }
        """

    def has_permission(self):
        try:
            # Check if the user has the correct group membership to access the data. To do this, we have to
            # decompose the ivoid into a namespace and a name.
            #
            namespace = name = None
            ivoid = self.service.get('parameters', {}).get('ID')
            ivoid_to_namespace_regex = self.service.get('attributes', {}).get('ivoid_to_namespace_regex')
            ivoid_to_name_regex = self.service.get('attributes', {}).get('ivoid_to_name_regex')
            if not ivoid_to_namespace_regex or not ivoid_to_name_regex:
                self.logger.debug("No ivoid to namespace/name decomposition regex specified in service attributes, "
                                  "attempting fallback...")
                for func in fallback_ivoid_decomposition_functions:
                    result = func(ivoid)
                    if not result:
                        continue
                    namespace, name = result
            else:
                namespace_match = re.search(ivoid_to_namespace_regex, ivoid)
                name_match = re.search(ivoid_to_name_regex, ivoid)
                if namespace_match and name_match:
                    namespace = namespace_match.group(1)
                    name = name_match.group(1)
                else:
                    return False
                
            # Add this plugin's required groups.
            self.required_groups.append([
                '/'.join([self.data_root_group, "namespaces", namespace])
            ])

            # Evaluate permission.
            return self.evaluate_permission()
        except AssertionError as e:
            self.logger.info(e)
            return False
        except Exception as e:
            self.logger.warning(e)
            return False
