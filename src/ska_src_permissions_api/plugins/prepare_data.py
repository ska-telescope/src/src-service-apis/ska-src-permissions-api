from ska_src_permissions_api.plugins.plugin import PermissionPlugin


class PrepareDataServicePermissionPlugin(PermissionPlugin):
    """ A plugin for the prepare_data service. """
    def __init__(self, services_root_group, data_root_group, groups, scopes, params):
        super().__init__(services_root_group, data_root_group, groups, scopes, params, add_common_required_groups=True)

    def _handle_get(self):
        """ Handle GET requests.

        A GET request expects the following body:

        {
          "service": {
            "node": site name from site-capabilities API
            "type": service type from site-capabilities API
            "uuid": service uuid from site-capabilities API
            "method": request method
          }
        }

        e.g.

        {
          "service": {
            "node": "SKAOSRC",
            "type": "prepare_data",
            "uuid": "197b9d57-34ba-4e30-a267-1b0aefc57aba",
            "method": "GET"
          }
        }
        """
        # This route has no additional group access restrictions as it a) contains no sensitive data and b) a uuid
        # would have to be guessed for it to return anything anyway.
        pass

    def _handle_post(self):
        """ Handle POST requests.

        A POST request expects the following body:

        {
          "service": {
            "node": site name from site-capabilities API
            "type": service type from site-capabilities API
            "uuid": service uuid from site-capabilities API
            "method": request method
            "body": request body
          }
        }

        e.g.

        {
          "service": {
            "node": "SKAOSRC",
            "type": "prepare_data",
            "uuid": "197b9d57-34ba-4e30-a267-1b0aefc57aba",
            "method": "POST",
            "body": [["testing:test.txt", "/path/to/rse/testing/test.txt", "./testing"]]
          }
        }
        """
        # List of (did, abs_path_to_src_file, rel_path_to_dst_dir_within_user_area) tuples
        # Example: [("testing:test.txt", "/path/to/rse/testing/test.txt", "./testing")]
        data_to_prepare: list[tuple[str, str, str]] = self.service.get("body", [])

        namespaces = set()
        for did in (did for did, _, _ in data_to_prepare):
            n_colons = did.count(":")
            assert n_colons == 1, f"DID [{did}] has unexpected format: expected exactly one colon, found {n_colons}"
            namespaces.add(did.split(":")[0])

        # Add this plugin's required groups.
        #
        # We check if the user is a member of all Rucio namespace groups in the set of namespaces from the DIDs in the
        # request.
        for namespace in namespaces:
            self.required_groups.append(['/'.join([self.data_root_group, "namespaces", namespace])])

    def has_permission(self):
        try:
            # Retrieve the method of the request and check permission based on this value.
            request_method = self.service.get('method')

            #FIXME: commenting for now as this isn't passed through in older versions of GK
            #assert request_method is not None, "No method specified for service."
            if request_method == 'GET':
                self._handle_get()
            elif request_method == 'POST':
                self._handle_post()

            # Evaluate permission.
            return self.evaluate_permission()
        except AssertionError as e:
            self.logger.info(e)
            return False
        except Exception as e:
            self.logger.warning(e)
            return False
