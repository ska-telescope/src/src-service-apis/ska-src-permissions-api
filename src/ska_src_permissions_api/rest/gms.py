import logging
import os
from string import Template

from fastapi import Request, HTTPException

from ska_src_permissions_api.client.iam import IamClient
from ska_src_permissions_api.common.exceptions import handle_exceptions


class Gms:
    def __init__(self, iam_client: IamClient, config_relpath: str):
        self.logger = logging.getLogger("uvicorn")
        self.iam = iam_client
        self.config_relpath = config_relpath

        # create capabilities.xml template for substitution in /capabilities route call
        capabilities_xml_path = os.path.join(self.config_relpath, 'capabilities.xml')
        try:
            with open(capabilities_xml_path, 'r') as file:
                file_content = file.read()
                self.capabilities_template = Template(file_content)
        except FileNotFoundError:
            raise FileNotFoundError("IVOA GMS configuration file not found at {}".format(capabilities_xml_path))

    async def get_groups(self, request: Request) -> str:
        """Get groups from IAM.

        :param request: FastAPI request object
        :return: List of groups
        """
        token = request.headers.get("Authorization")
        if token and token.startswith("Bearer "):
            token = token[7:]
        else:
            raise HTTPException(status_code=401, detail="Invalid access token")
        groups = request.query_params.getlist("group")
        groups = set(groups) if groups else set()

        user_info = await self.iam.fetch_userinfo(token=token)

        user = await self.iam.fetch_user_by_id(user_id=user_info["sub"])
        self.logger.debug(f"user is {user}")

        return self.get_matching_groups(user_profile=user, requested_groups=groups)

    def get_matching_groups(self, user_profile: dict, requested_groups: set) -> str:
        """Get the matching groups from user profile for given requested groups.

        :param JSON user_profile: User profile from IAM
        :param str requested_groups: Requested groups
        :return: str: Matching groups
        """
        profile_group_list = user_profile["groups"]
        if not profile_group_list or len(profile_group_list) == 0:
            return ""

        if not requested_groups:
            groups = ""
            for group in profile_group_list:
                groups += group["display"] + "\n"
            return groups

        matching_groups = [
            group["display"] for group in profile_group_list if group["display"] in requested_groups
        ]

        self.logger.debug(f"matching groups are {matching_groups}")
        return "" if len(matching_groups) == 0 else "\n".join(matching_groups) + "\n"
