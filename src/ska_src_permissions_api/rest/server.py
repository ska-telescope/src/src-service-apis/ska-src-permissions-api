import asyncio
import base64
import logging
import os
import time
from typing import Union
import jwt
import requests
import rsa
from authlib.integrations.requests_client import OAuth2Session
from fastapi import FastAPI, Depends, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi_versioning import VersionedFastAPI, version
from starlette.config import Config
from starlette.requests import Request
from starlette.responses import JSONResponse, Response

from ska_src_permissions_api.client.iam import IamClient
from ska_src_permissions_api.common import constants
from ska_src_permissions_api.common.exceptions import (handle_exceptions, NoPublicKeyForToken, TokenInactive,
                                                       TokenMissing)
from ska_src_permissions_api.rest.gms import Gms
from ska_src_permissions_api.rest.permissions import Permission

config = Config('.env')

# Instantiate FastAPI() allowing CORS. Middleware is added later to the instance of a VersionedApp.
#
app = FastAPI()
CORSMiddleware_params = {
    "allow_origins": ["*"],
    "allow_credentials": True,
    "allow_methods": ["*"],
    "allow_headers": ["*"]
}

# Set logging to use uvicorn logger.
#
logger = logging.getLogger("uvicorn")
# Instantiate OAuth2 request sessions for the permissions api.
#
API_IAM_CLIENT = OAuth2Session(config.get('API_IAM_CLIENT_ID'),
                               config.get('API_IAM_CLIENT_SECRET'),
                               scope=config.get('API_IAM_CLIENT_SCOPES', default=""))

# Get instance of IAM constants.
#
IAM_CONSTANTS = constants.IAM(client_conf_url=config.get('IAM_CLIENT_CONF_URL'))

# Instantiate Permission module.
#
PERMISSION = Permission(config.get('SCHEMAS_RELPATH'),
                        config.get('PERMISSIONS_SERVICES_ROOT_GROUP_NAME', default=""),
                        config.get('PERMISSIONS_DATA_ROOT_GROUP_NAME', default=""),
                        config.get('PERMISSIONS_RELPATH'),
                        config.get('PERMISSIONS_INCLUDE_POLICIES_FQN'))

# Instantiate IAM client.
#
IAM_CLIENT = IamClient(iam_constants=IAM_CONSTANTS,
                       client_id=config.get('API_IAM_CLIENT_ID'),
                       client_secret=config.get('API_IAM_CLIENT_SECRET'),
                       api_timeout=config.get('API_IAM_CLIENT_TIMEOUT_S', default=10))

# Instantiate IVOA GMS module.
#
GMS = Gms(iam_client=IAM_CLIENT, config_relpath=config.get('IVOA_GMS_CONFIG_RELPATH'))

# Store service start time.
#
SERVICE_START_TIME = time.time()

# Keep track of number of managed requests.
#
REQUESTS_COUNTER = 0
REQUESTS_COUNTER_LOCK = asyncio.Lock()

# Get public keys from IAM (for offline token validation).
#
IAM_JWKSET_PUBKEYS = {}
if config.get('VALIDATE_TOKENS_OFFLINE', default='false') == 'true':
    logger.info("Validating tokens OFFLINE.")

    # Get the public jwkset from IAM.
    req = requests.Request('GET', IAM_CONSTANTS.iam_endpoint_jwk_uris).prepare()
    resp = requests.Session().send(req)
    resp.raise_for_status()
    jwkset = resp.json()

    # Generate PEM public keys from this jwkset.
    for key in jwkset.get('keys'):
        modulus = int.from_bytes(base64.urlsafe_b64decode(key['n'] + '=='), 'big')
        exponent = int.from_bytes(base64.urlsafe_b64decode(key['e'] + '=='), 'big')
        public_key = rsa.PublicKey(modulus, exponent).save_pkcs1(format='PEM')
        IAM_JWKSET_PUBKEYS[key.get('kid')] = public_key
else:
    logger.info("Validating tokens ONLINE.")

# Dependencies.
# -------------
#
# Increment the request counter.
#
@handle_exceptions
async def increment_request_counter(request: Request) -> Union[dict, HTTPException]:
    global REQUESTS_COUNTER
    async with REQUESTS_COUNTER_LOCK:
        REQUESTS_COUNTER += 1


# Introspect a token for validity and group membership information.
#
# Supports both online (using introspection endpoint) and offline validation (using jwksets).
#
@handle_exceptions
async def introspect_token(token: Union[str, None] = None):
    """
    Introspect and validate a token using the IAM introspection endpoint.
    """
    introspected_token = None
    if token:
        if IAM_JWKSET_PUBKEYS:
            # Decode token for header (unverified).
            decoded_token = jwt.api_jwt.decode_complete(token, options={"verify_signature": False})
            decoded_token_header = decoded_token.get('header')

            # Get the appropriate public key for this header.
            matched_public_key = None
            for kid, public_key in IAM_JWKSET_PUBKEYS.items():
                if decoded_token_header.get('kid') == kid:
                    matched_public_key = public_key
                    break
            if not matched_public_key:
                raise NoPublicKeyForToken

            # Decode token (verified).
            try:
                introspected_token = jwt.decode(token,
                   key=matched_public_key,
                   algorithms=[decoded_token_header.get('alg')],
                   options={
                       'verify_aud': False,
                       'verify_iss': False
                   })
            except jwt.ExpiredSignatureError as e:
                raise TokenInactive
        else:
            resp = API_IAM_CLIENT.introspect_token(IAM_CONSTANTS.iam_endpoint_introspection, token=token,
                                                   token_type_hint='access_token')
            resp.raise_for_status()
            introspected_token = resp.json()
            if introspected_token.get('active', False) is not True:
                raise TokenInactive
    else:
        raise TokenMissing
    return introspected_token


# Routes
# ------
#
@app.post('/authorise/exchange/{service}', dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def authorise_service_exchange(request: Request, service: str, version: str = 'latest',
                                     token: Union[str, None] = None,
                                     introspected_token: Union[str, None] = Depends(introspect_token)) \
        -> Union[JSONResponse, HTTPException]:
    """
    Authorise access for a token exchange between services based on a user's group membership. This is used by
    policies with type 'exchange'.
    """
    # Get introspected token groups, audience and issuer.
    groups = introspected_token.get('wlcg.groups', [])
    if not groups:
        groups = introspected_token.get('groups', [])
    groups = ['/{}'.format(group) for group in groups if not group.startswith('/')]     # maintain consistency with wlcg token profile
    try:
        issuer = introspected_token.get('iss')
        audience = PERMISSION.check_permission_for_exchange(service=service, version=version, groups=groups,
                                                            issuer=issuer)
        if audience:
            return JSONResponse({
                "is_authorised": True,
                "audience": audience
            })
        return JSONResponse({"is_authorised": False})
    except Exception as e:
        if hasattr(e, "http_error_status"):
            raise HTTPException(e.http_error_status, e.message)
        raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR, repr(e))


@app.post('/authorise/route/{service}', dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def authorise_service_route(request: Request, service: str, route: str, method: str, version: str = 'latest',
                                  body: dict = {}, token: Union[str, None] = None,
                                  introspected_token: Union[str, None] = Depends(introspect_token)) \
        -> Union[JSONResponse, HTTPException]:
    """
    Authorise access to a service's route based on a user's group membership. This is used by policies with
    type 'route'.
    """
    # Get introspected token groups, audience and issuer.
    groups = introspected_token.get('wlcg.groups', [])
    if not groups:
        groups = introspected_token.get('groups', [])
    groups = ['/{}'.format(group) for group in groups if not group.startswith('/')]     # maintain consistency with wlcg token profile
    audience = introspected_token.get('aud', "")
    scope = introspected_token.get('scope', "")
    issuer = introspected_token.get('iss')

    # Check permission using policy and user group membership.
    try:
        if PERMISSION.check_permission_for_route(service=service, version=version, route=route, method=method,
                                                 groups=groups, scopes=scope, audience=audience, issuer=issuer,
                                                 params=body):
            return JSONResponse({"is_authorised": True})
        return JSONResponse({"is_authorised": False})
    except Exception as e:
        if hasattr(e, "http_error_status"):
            raise HTTPException(e.http_error_status, e.message)
        raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR, repr(e))


@app.post('/authorise/plugin/{service}', dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def authorise_service_by_plugin(request: Request, service: str, version: str = 'latest', body: dict = {},
                                      token: Union[str, None] = None, 
                                      introspected_token: Union[str, None] = Depends(introspect_token)) \
        -> Union[JSONResponse, HTTPException]:
    """
    Authorise access to a service by plugin. This is used by policies with type 'plugin'.
    """
    # Get introspected token groups, scope and issuer.
    groups = introspected_token.get('wlcg.groups', [])
    if not groups:
        groups = introspected_token.get('groups', [])
    groups = ['/{}'.format(group) for group in groups if not group.startswith('/')]     # maintain consistency with wlcg token profile
    scope = introspected_token.get('scope', "")
    issuer = introspected_token.get('iss')

    # Check permission using policy and user group membership.
    try:
        if PERMISSION.check_permission_by_plugin(service=service, version=version, groups=groups, scopes=scope,
                                                 issuer=issuer, params=body):
            return JSONResponse({"is_authorised": True})
        return JSONResponse({"is_authorised": False})
    except Exception as e:
        if hasattr(e, "http_error_status"):
            raise HTTPException(e.http_error_status, e.message)
        raise HTTPException(status.HTTP_500_INTERNAL_SERVER_ERROR, repr(e))


@app.get("/policies", response_class=JSONResponse, dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def list_policies(request: Request) -> JSONResponse:
    """ Retrieve a list of permission policies. """
    return JSONResponse(PERMISSION.list_policies())


@app.get("/policies/types", response_class=JSONResponse, dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def list_policy_types(request: Request) -> JSONResponse:
    """ Retrieve a list of permission policy types. """
    return JSONResponse(PERMISSION.list_policy_types())


@app.get("/policies/{type}/", response_class=JSONResponse, dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def list_policies_by_type(request: Request, type: str) -> JSONResponse:
    """ Retrieve a list of permission policies by the service type. """
    return JSONResponse(PERMISSION.list_permission_polices_for_type(type))


@app.get("/policies/{type}/{service}", response_class=JSONResponse, dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def list_policies_by_service_name(request: Request, type: str, service: str) -> JSONResponse:
    """ Retrieve a list of permission policies by the service name. """
    return JSONResponse(PERMISSION.list_permission_polices_for_type_and_service(type, service))


@app.get("/policies/{type}/{service}/{version}", response_class=JSONResponse,
         dependencies=[Depends(increment_request_counter)])
@handle_exceptions
@version(1)
async def get_policy_by_service_name_and_version(request: Request, type: str, service: str, version: str) \
        -> JSONResponse:
    """ Get a permission policy by the service name and version. """
    return JSONResponse(PERMISSION.get_permission_polices_for_type_and_service_and_version(type, service, version))


@app.get('/gms/search', responses={
    200: {"content": "text/plain", "description": "Groups found"},
    500: {"description": "Internal Server Error"},
    401: {"content": "application/json", "description": "Unauthorized access"}
})
@version(1)
@handle_exceptions
async def search_groups(request: Request):
    """Get groups for user from IAM."""
    try:
        groups = await GMS.get_groups(request)
        return Response(
            content=groups,
            media_type="text/plain")
    except Exception as e:
        logger.error(f"Error in /gms/search: {e}")
        raise e


@app.get('/gms/capabilities', responses={
    200: {"content": "application/xml"},
    500: {"description": "Internal Server Error"}
})
@version(1)
@handle_exceptions
async def capabilities(request: Request):
    """ Retrieves the capabilities xml """
    full_url = f"{request.url.scheme}://{request.url.hostname}"
    if request.url.port:
        full_url += f":{request.url.port}"
    full_url += config.get(key='API_ROOT_PATH', default='')
    return Response(
        content=GMS.capabilities_template.substitute(fullUrl=full_url),
        media_type="application/xml",
    )


@app.get('/ping')
@handle_exceptions
@version(1)
async def ping(request: Request):
    """ Service aliveness. """
    return JSONResponse({
        'status': "UP",
        'version': os.environ.get('SERVICE_VERSION'),
    })


@app.get('/health')
@handle_exceptions
@version(1)
async def health(request: Request):
    """ Service health. """

    # Dependent services.
    #
    # IAM
    #
    # Prepare REST request and send.
    req = requests.Request('GET',  os.path.join(config.get('IAM_CLIENT_CONF_URL').split('.well-known')[0],
                                                'actuator/health')).prepare()
    indigo_iam_api_response = requests.Session().send(req)

    # Set return code dependent on criteria e.g. dependent service statuses
    #
    healthy_criteria = [
        indigo_iam_api_response.status_code == 200
    ]
    return JSONResponse(
        status_code=status.HTTP_200_OK if all(healthy_criteria) else status.HTTP_500_INTERNAL_SERVER_ERROR,
        content={
            'uptime': round(time.time() - SERVICE_START_TIME),
            'number_of_managed_requests': REQUESTS_COUNTER,
            'dependent_services': {
                'iam': {
                    'status': 'UP' if indigo_iam_api_response.status_code == 200 else 'DOWN'
                }
            }
        }
    )



app = VersionedFastAPI(app, version_format='{major}', prefix_format='/v{major}')
app.add_middleware(CORSMiddleware, **CORSMiddleware_params)