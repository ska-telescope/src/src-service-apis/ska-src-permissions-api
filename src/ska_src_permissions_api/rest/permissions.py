import glob
import importlib
import json
import os
from distutils.util import strtobool

import jsonschema

from ska_src_permissions_api.common.exceptions import (MissingPermissionKeyword, NoPermissionDefinitionForRoute, \
    PermissionExpressionMalformed, NoPermissionPolicyForService, NoPermissionPolicyForServiceVersion, \
    NoPermissionPolicyForType, PermissionPolicySchemaNotFoundForPolicyType, PermissionPolicyMalformed, \
    PermissionPolicySchemaMalformed, PermissionPolicySchemaValidationFailed, PermissionPluginNotFound, \
    TokenInvalidAudience, TokenInvalidIssuer)


class Permission:
    def __init__(self, schemas_relpath, permissions_services_root_group_name, permissions_data_root_group_name,
                 permissions_relpath, permissions_include_policies_fqn):
        """ Class for handling permissions.

        For a policy to be included in the permissions service it must first be matched and validated against the
        corresponding schema in the schemas `permission_policies` subdirectory.

        To be matched to a schema, the permission policy `type` attribute must match the name of one of the files in
        this subdirectory (without file extension suffix).
        """
        permissions_policies_schemas_relpath = os.path.join(schemas_relpath, 'permission_policies')
        self.permissions_services_root_group_name = permissions_services_root_group_name
        self.permissions_data_root_group_name = permissions_data_root_group_name
        # Process permissions policies.
        #
        self._permissions_policies = {}
        for policy_fqn in permissions_include_policies_fqn.split(','):
            policy_relpath = permissions_relpath
            for policy_part_idx, policy_part in enumerate(policy_fqn.strip().split('.')):
                policy_relpath = os.path.join(policy_relpath, policy_part)
            for policy_json in glob.glob("{}/*.json".format(policy_relpath)):
                # try loading the policy json
                with open(policy_json, 'r') as f:
                    try:
                        policy = json.load(f)
                    except json.decoder.JSONDecodeError as e:
                        raise PermissionPolicyMalformed(policy_relpath, e)

                # check policy type is set
                policy_type = policy.get('type')
                if not policy_type:
                    raise PermissionPolicyMalformed(policy_relpath)

                # check policy against schema
                policy_schema_definition_abspath = os.path.join(
                    permissions_policies_schemas_relpath, '{}.json'.format(policy_type)
                )
                if not os.path.exists(policy_schema_definition_abspath):
                    raise PermissionPolicySchemaNotFoundForPolicyType(policy_type)
                with open(policy_schema_definition_abspath) as f:
                    try:
                        policy_schema = json.load(f)
                    except json.decoder.JSONDecodeError as e:
                        raise PermissionPolicySchemaMalformed(policy_type, e)
                try:
                    jsonschema.validate(policy, policy_schema)
                except jsonschema.exceptions.SchemaError as e:
                    raise PermissionPolicySchemaValidationFailed(policy_relpath, e)

                # assign common variables for all policies
                policy_name = policy.get('name')
                policy_version = "{}".format(policy.get('version_number'))
                policy_description = policy.get('description')

                # add entry to _permissions_policies for this policy type if it doesn't already exist.
                if policy_type not in self._permissions_policies:
                    self._permissions_policies[policy_type] = {}

                # add entry to _permissions_policies for this type, service and version & add description
                if policy_name not in self._permissions_policies:
                    self._permissions_policies[policy_type][policy_name] = {}
                if policy_version not in self._permissions_policies[policy_type][policy_name]:
                    self._permissions_policies[policy_type][policy_name][policy_version] = {}
                self._permissions_policies[policy_type][policy_name][policy_version]['description'] \
                    = policy_description

                # policy type specific logic
                if policy_type == 'route':
                    self._permissions_policies[policy_type][policy_name][policy_version]['iam_subgroup_name'] \
                        = policy.get('iam_subgroup_name')
                    self._permissions_policies[policy_type][policy_name][policy_version]['expected_token_issuer'] \
                        = policy.get('expected_token_issuer')
                    self._permissions_policies[policy_type][policy_name][policy_version]['expected_service_token_scope'] \
                        = policy.get('expected_service_token_scope')
                    self._permissions_policies[policy_type][policy_name][policy_version]['expected_token_audience'] \
                        = policy.get('expected_token_audience')
                    self._permissions_policies[policy_type][policy_name][policy_version]['roles'] \
                        = policy.get('roles')
                    self._permissions_policies[policy_type][policy_name][policy_version]['routes'] \
                        = policy.get('routes')
                elif policy_type == 'exchange':
                    self._permissions_policies[policy_type][policy_name][policy_version]['iam_subgroup_name'] \
                        = policy.get('iam_subgroup_name')
                    self._permissions_policies[policy_type][policy_name][policy_version]['expected_token_issuer'] \
                        = policy.get('expected_token_issuer')
                    self._permissions_policies[policy_type][policy_name][policy_version]['token_audience'] \
                        = policy.get('token_audience')
                elif policy_type == 'plugin':
                    self._permissions_policies[policy_type][policy_name][policy_version]['iam_subgroup_name'] \
                        = policy.get('iam_subgroup_name')
                    self._permissions_policies[policy_type][policy_name][policy_version]['expected_token_issuer'] \
                        = policy.get('expected_token_issuer')
                    self._permissions_policies[policy_type][policy_name][policy_version]['plugin_module_name'] \
                        = policy.get('plugin_module_name')
                    self._permissions_policies[policy_type][policy_name][policy_version]['plugin_class_name'] \
                        = policy.get('plugin_class_name')

    def _check_group_membership_for_role(self, service_group, data_root_group, role_expression, roles, groups, params):
        """ Check if a user has the required group membership to be assigned one of the required roles. """
        required_role_groups = roles[role_expression]
        for required_role_group in required_role_groups:
            try:
                if required_role_group.format(
                        service_group=service_group, data_root_group=data_root_group, **params) in groups:
                    return True
                return False
            except KeyError as e:
                raise MissingPermissionKeyword(repr(e.args))
        return True

    def _check_token_audience(self, presented, expected):
        """ Verify that the audience in the token matches that expected by the policy. """
        if expected not in presented.split():
            raise TokenInvalidAudience(presented, expected)

    def _check_token_issuer(self, presented, expected):
        """ Verify that the issuer of the token matches that expected by the policy. """
        presented = presented.rstrip('/')
        expected = expected.rstrip('/')
        if expected != presented:
            raise TokenInvalidIssuer(presented, expected)

    def check_permission_for_exchange(self, service, version, groups, issuer):
        """ Check whether a user has permission to exchange a token for access to a particular service given their group
        membership.

        For token exchange authorisation, the user’s groups are identified and membership is checked against the
        policy's {permissions_services_root_group_name}/{iam_subgroup_name} where {permissions_services_root_group_name}
        is a constant set at application run-time.

        If authorisation is successful, the audience for the service (taken from the policy) is returned.

        The issuer of the token is first checked against that expected from the policy. The audience of the token is
        not checked against any expected audience.
        """
        # Filter permissions policies by type 'exchange'
        permissions_policies_by_service = self._permissions_policies.get('exchange', {})

        # Get permission policy for the requested service.
        if not permissions_policies_by_service.get(service):
            raise NoPermissionPolicyForService
        if version == 'latest':
            version_number = sorted(list(permissions_policies_by_service.get(service, {}).keys()))[-1]
        elif version.startswith('v'):
            version_number = version.lstrip('v')
        else:
            version_number = version
        permissions_policy = permissions_policies_by_service.get(service, {}).get(version_number)
        if not permissions_policy:
            raise NoPermissionPolicyForServiceVersion

        # Check the token issuer.
        expected_token_issuer = permissions_policy.get('expected_token_issuer')
        self._check_token_issuer(issuer, expected_token_issuer)

        # Check the permission.
        service_group = '/{}/{}'.format(self.permissions_services_root_group_name.strip('/'),
                                        permissions_policy.get("iam_subgroup_name").strip('/'))
        if service_group in groups:
            return permissions_policy.get("token_audience")
        return False

    def check_permission_for_route(self, service, version, route, method, groups, scopes, audience, issuer, params):
        """ Check whether a user has permission to access a particular service's route given the token scope and
        IAM group membership.

        For route based authorisation, permission can be granted by verifying EITHER that the token contains a
        particular scope, or that the user is a member of a necessary group.

        Scope checks are straightforward. The token must contain the expected service token scope from the policy.

        For group membership checks, the user’s groups are identified and a “role” is assigned. The permissions
        policy maps API routes to the concept of “roles”, restricting access based on method and, additionally,
        path, e.g. for get/set metadata in the default permissions package, the corresponding block looks something
        like:

        {
            "/metadata/{namespace}/{name}": {
                "GET": "admin or namespace-viewer or namespace-editor or namespace-owner",
                "POST": "admin or namespace-editor or namespace-owner"
            },
        }

        where the {namespace-viewer}, {namespace-editor} and {namespace-owner} roles are only assigned for users who
        have the following IAM group membership:

        {
            "namespace-viewer": [
                "{data_group}/namespaces/{namespace}/viewer"
            ],
            "namespace-editor": [
                "{data_group}/namespaces/{namespace}/editor"
            ],
            "namespace-owner": [
                "{data_group}/namespaces/{namespace}/owner"
            ],
        }

        So that the required group membership to access the get metadata endpoint for the e.g. {testing} namespace would
        look something like:

        /data/namespaces/
            testing/
                viewer||editor||owner

        and likewise for the set metadata endpoint:

        /data/namespaces/
            testing/
                editor||owner

        Roles are assigned when a request to a particular endpoint is made. This enables information from the request to
        be used to understand if a role can be assigned. For example, consider the {namespace-viewer} role:

        "namespace-viewer": [
            "{data_group}/namespaces/{namespace}/viewer"
        ],

        which requires both {data_group} and {namespace} to be provided. The {data_group} is an application specific
        parameter set at run-time (permissions_services_root_group_name), but the {namespace} parameter is substituted
        when the request is made. In the case of a GET request for metadata, the route /metadata/{namespace}/{name}
        provides the {namespace} as a path parameter, and this value is substituted into the role definition. The
        source of the substitution for the role definition depends on either the path parameters, query parameters or
        body of the request; which are used depends on where the parameters are expected to come from.

        The issuer and audience of the token are first checked against that expected from the policy.
        """
        # Filter permissions policies by type 'route'
        permissions_policies_by_route = self._permissions_policies.get('route', {})

        # Get permission policy for the requested service and route.
        if not permissions_policies_by_route.get(service):
            raise NoPermissionPolicyForService
        if version == 'latest':
            version_number = sorted(list(permissions_policies_by_route.get(service, {}).keys()))[-1]
        elif version.startswith('v'):
            version_number = version.lstrip('v')
        else:
            version_number = version
        permissions_policy = permissions_policies_by_route.get(service, {}).get(version_number)
        if not permissions_policy:
            raise NoPermissionPolicyForServiceVersion

        # Check the token issuer.
        expected_token_issuer = permissions_policy.get('expected_token_issuer')
        self._check_token_issuer(issuer, expected_token_issuer)

        # Check the token audience.
        expected_token_audience = permissions_policy.get('expected_token_audience')
        self._check_token_audience(audience, expected_token_audience)

        # Check permission by scope.
        expected_service_token_scope = permissions_policy.get('expected_service_token_scope')
        if expected_service_token_scope in scopes.split():
            return True

        # Check permission by groups.
        routes = permissions_policy.get('routes')
        roles = permissions_policy.get('roles')
        service_group = '/{}/{}'.format(self.permissions_services_root_group_name.strip('/'),
                                        permissions_policy.get("iam_subgroup_name").strip('/'))
        data_root_group = '/{}'.format(self.permissions_data_root_group_name.strip('/'))
        evaluated_permission_expression = []            # list to store all the different clauses in a permission expr
        if route in routes:
            if method in routes[route]:
                role_expression = str(routes[route][method])
                for role_expression_token in role_expression.split():
                    role_expression_token = role_expression_token.strip().rstrip('/')
                    if role_expression_token in roles:
                        evaluated_permission_expression.append(
                            str(self._check_group_membership_for_role(
                                service_group=service_group, data_root_group=data_root_group,
                                role_expression=role_expression_token,
                                roles=roles, groups=groups, params=params)))
                    elif role_expression_token.lower() in ['and', 'or', 'not']:
                        evaluated_permission_expression.append(role_expression_token)
                    elif role_expression_token.lower() in ['true', 'false']:
                        evaluated_permission_expression.append(str(strtobool(role_expression_token)))
                    else:
                        raise PermissionExpressionMalformed(role_expression)
                try:
                    has_permission = eval(' '.join(evaluated_permission_expression))
                except (NameError, SyntaxError):
                    raise PermissionExpressionMalformed(evaluated_permission_expression)
                if not has_permission:
                    return False
                return True
            raise NoPermissionDefinitionForRoute
        raise NoPermissionDefinitionForRoute

    def check_permission_by_plugin(self, service, version, groups, scopes, issuer, params):
        """ Check whether a user has permission to use a service by passing the token to a custom plugin.

        The issuer and audience of the token are first checked against that expected from the policy.
        """

        # Filter permissions policies by type 'plugin'
        permissions_policies_by_service = self._permissions_policies.get('plugin', {})

        # Get permission policy for the requested service.
        if not permissions_policies_by_service.get(service):
            raise NoPermissionPolicyForService
        if version == 'latest':
            version_number = sorted(list(permissions_policies_by_service.get(service, {}).keys()))[-1]
        elif version.startswith('v'):
            version_number = version.lstrip('v')
        else:
            version_number = version
        permissions_policy = permissions_policies_by_service.get(service, {}).get(version_number)
        if not permissions_policy:
            raise NoPermissionPolicyForServiceVersion

        # Check the token issuer.
        expected_token_issuer = permissions_policy.get('expected_token_issuer')
        self._check_token_issuer(issuer, expected_token_issuer)

        # Check permission by scope.
        expected_service_token_scope = permissions_policy.get('expected_service_token_scope')
        if expected_service_token_scope in scopes.split():
            return True

        # Try to import the corresponding permissions plugin and instantiate.
        plugin_module_name = permissions_policy.get('plugin_module_name')
        plugin_class_name = permissions_policy.get('plugin_class_name')
        try:
            plugin_module = importlib.import_module(plugin_module_name)
        except ModuleNotFoundError:
            raise PermissionPluginNotFound(plugin_module_name)
        plugin_class = getattr(plugin_module, plugin_class_name)

        services_root_group = "/{}".format(self.permissions_services_root_group_name.strip('/'))
        data_root_group = "/{}".format(self.permissions_data_root_group_name.strip('/'))

        plugin = plugin_class(services_root_group=services_root_group, data_root_group=data_root_group, groups=groups,
                              scopes=scopes, params=params)

        # Check the permission.
        return plugin.has_permission()

    def get_permission_polices_for_type_and_service_and_version(self, type, service, version):
        """ Get a permission policy given a type, service and version. """
        if not self.permission_policies.get(type):
            raise NoPermissionPolicyForType
        if not self.permission_policies.get(type).get(service):
            raise NoPermissionPolicyForService
        if version == 'latest':
            version_number = sorted(list(self.permission_policies.get(type).get(service).keys()))[-1]
        elif version.startswith('v'):
            version_number = version.lstrip('v')
        else:
            version_number = version
        if not self.permission_policies.get(type).get(service).get(version_number):
            raise NoPermissionPolicyForServiceVersion
        return self.permission_policies.get(type).get(service).get(version_number)

    def list_permission_polices_for_type(self, type):
        """ List permission policies for a given type. """
        if not self.permission_policies.get(type):
            raise NoPermissionPolicyForType
        policies = {}
        for name, policy in self.permission_policies.get(type, {}).items():
            policies[name] = {'versions': list(policy.keys())}
        return policies

    def list_permission_polices_for_type_and_service(self, type, service):
        """ List permission policies for a given type and service. """
        if not self.permission_policies.get(type):
            raise NoPermissionPolicyForType
        if not self.permission_policies.get(type).get(service):
            raise NoPermissionPolicyForService
        return self.permission_policies.get(type).get(service)

    def list_policies(self):
        """ List permission policies. """
        policies = {}
        for type, policy_by_service in self.permission_policies.items():
            policies[type] = {}
            for service, policy_by_version in policy_by_service.items():
                policies[type][service] = {'versions': list(policy_by_version.keys())}
        return policies

    def list_policy_types(self):
        """ List permission policy types. """
        return list(self.permission_policies.keys())

    @property
    def permission_policies(self):
        return self._permissions_policies
