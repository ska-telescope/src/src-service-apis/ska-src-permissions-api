import logging

import httpx
from fastapi import HTTPException


class IamClient:
    def __init__(self, iam_constants=None, client_id=None, client_secret=None, api_timeout=10):
        self.logger = logging.getLogger("uvicorn")
        self.iam_constants = iam_constants
        self.client_id = client_id
        self.client_secret = client_secret
        self.api_timeout = float(api_timeout)
        self.client = httpx.AsyncClient()

    async def __request__(self, url: str, method: str, headers=None, data=None, timeout=10.0):
        try:
            response = await self.client.request(url=url, method=method, headers=headers, data=data, timeout=timeout)
            response.raise_for_status()  # Raises an error for bad responses
            return response.json()
        except httpx.TimeoutException as exc:
            raise HTTPException(status_code=504, detail=f"backend timed out for {exc.request.url}")
        except httpx.RequestError as exc:
            raise HTTPException(status_code=500, detail=f"Error while requesting {exc.request.url}")
        except httpx.HTTPStatusError as exc:
            raise HTTPException(status_code=exc.response.status_code, detail=str(exc))

    async def fetch_userinfo(self, token: str) -> dict:
        """Fetch userinfo from IAM.

        :return: dict: Userinfo
        :rtype: dict
        :param: str token: Access token
        :raises: HTTPError: If the request fails from iam
        """
        headers = {
            "Authorization": f"Bearer {token}",
            "Accept": "*/*",
        }
        user_info_url = self.iam_constants.iam_endpoint_userinfo
        user_info = await self.__request__(url=user_info_url, method="GET", headers=headers, timeout=self.api_timeout)
        return user_info

    async def __fetch_client_credential__(self):
        """Fetch client credential token from IAM.

        :return: dict: Client credential token
        :rtype: dict
        :raises: HTTPError: If the request fails from iam
        """

        token_url = self.iam_constants.iam_endpoint_token
        data = {
            "grant_type": "client_credentials",
            "client_id": self.client_id,
            "client_secret": self.client_secret
        }
        client_credential_token_data = await self.__request__(url=token_url, method="POST", data=data,
                                                              timeout=self.api_timeout)
        return client_credential_token_data

    async def fetch_user_by_id(self, user_id: str) -> dict:
        """Fetch user by id from IAM.

        :return: JSON: User
        :rtype: dict
        :param: str user_id: User id
        :raises: HTTPError: If the request fails from iam
        """

        client_credential_token_data = await self.__fetch_client_credential__()
        access_token = client_credential_token_data["access_token"]

        headers = {
            "Authorization": f"Bearer {access_token}",
            "Accept": "*/*",
        }
        scim_url = self.iam_constants.iam_endpoint_scim
        user_profile_url = f"{scim_url}/Users/{user_id}"
        user_profile = await self.__request__(url=user_profile_url, method="GET", headers=headers,
                                              timeout=self.api_timeout)
        return user_profile
