import json
import requests
import warnings

from ska_src_permissions_api.common.exceptions import handle_client_exceptions


class PermissionsClient:
    def __init__(self, api_url, session=None):
        self.api_url = api_url
        if session:
            self.session = session
        else:
            self.session = requests.Session()

    @handle_client_exceptions
    def authorise_route_for_service(self, service: str, version: str, route: str, method: str, body: {} = {},
                                    token: str = None):
        warnings.warn("authorise_route_for_service is deprecated, please use authorise_service_route instead.",
                      DeprecationWarning)
        return self.authorise_service_route(service, version, route, method, body, token)

    @handle_client_exceptions
    def authorise_token_exchange(self, service: str, version: str, token: str = None):
        warnings.warn("authorise_token_exchange is deprecated, please use authorise_service_exchange instead.",
                      DeprecationWarning)
        return self.authorise_service_exchange(service, version, token)

    @handle_client_exceptions
    def authorise_route_by_plugin(self, service: str, version: str, body: {} = {}, token: str = None):
        """ Authorise access to a service by plugin.

        :param str service: The service name.
        :param str version: The service version.
        :param dict body: The request body containing any parameters.
        :param str token: An access token.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        authorise_route_by_plugin_endpoint = "{api_url}/authorise/plugin/{service}".format(
            api_url=self.api_url, service=service)
        params = {
            "version": version,
            "token": token,
        }
        resp = self.session.post(authorise_route_by_plugin_endpoint, params=params, data=json.dumps(body))
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def authorise_service_route(self, service: str, version: str, route: str, method: str, body: {} = {},
                                token: str = None):
        """ Authorise access to a service route.

        :param str service: The service name.
        :param str version: The service version.
        :param str route: The service's route.
        :param str method: The REST access method.
        :param dict body: The request body containing any parameters.
        :param str token: An access token.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        authorise_service_route_endpoint = "{api_url}/authorise/route/{service}".format(
            api_url=self.api_url, service=service)
        params = {
            "version": version,
            "route": route,
            "method": method,
            "token": token,
        }
        resp = self.session.post(authorise_service_route_endpoint, params=params, data=json.dumps(body))
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def authorise_service_exchange(self, service: str, version: str, token: str = None):
        """ Authorise a token exchange.

        :param str service: The service name.
        :param str version: The service version.
        :param str token: An access token.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        authorise_service_exchange_endpoint = "{api_url}/authorise/exchange/{service}".format(
            api_url=self.api_url, service=service, version=version)
        params = {
            "version": version,
            "token": token,
        }
        resp = self.session.post(authorise_service_exchange_endpoint, params=params)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def health(self):
        """ Get the service health.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        health_endpoint = "{api_url}/health".format(api_url=self.api_url)
        resp = self.session.get(health_endpoint)
        resp.raise_for_status()
        return resp

    @handle_client_exceptions
    def ping(self):
        """ Ping the service.

        :return: A requests response.
        :rtype: requests.models.Response
        """
        ping_endpoint = "{api_url}/ping".format(api_url=self.api_url)
        resp = self.session.get(ping_endpoint)
        resp.raise_for_status()
        return resp
