 # SKA SRC Permissions API

[[_TOC_]]

## Overview

This API exposes endpoints related to SRCNet permissions.

Permissions policies for the different services are stored in `etc/permissions/<service_name>/<service_version>`. 
These policies are loaded and validated against a schema at runtime. It is critical that each policy 
contains the `type` attribute so that the schema corresponding a policy can be correctly matched.

The schemas that these policies are evaluated against are stored in `etc/schemas/permission_policies`.

## Authorisation

Authorisation is managed using the concepts of groups, roles and permissions. IAM groups are bound to roles and roles 
are bound to permissions. How the latter is done depends on what policy `type` has been specified. Current supported 
types include:

- route
- exchange
- plugin

### Route based (`type=route`)

For route based authorisation, permission can be granted by verifying either that the token contains a particular scope, 
or that the user is a member of a necessary group.

Scope checks are straightforward. The token must contain the expected service token scope from the policy.

For group membership checks, a permission is defined by a route and a HTTP method. Note that subgroups within a 
group can be used to limit access further, for example, an endpoint that details information about a particular 
site `/sites/{site}` could be restricted to users in the corresponding `/roles/{site}/viewer` group.

Within the application itself, the user’s groups are identified and a “role” is assigned. The permissions
policy maps API routes to the concept of “roles”, restricting access based on method and, additionally,
path, e.g. for get/set metadata in the default permissions package, the corresponding block looks something
like:

```json
{
   "/metadata/{namespace}/{name}": {
      "GET": "admin or namespace-viewer or namespace-editor or namespace-owner",
      "POST": "admin or namespace-editor or namespace-owner"
   },
   ...
}
```

telling the application that for `GET` and `POST` requests to the route `/metadata/{namespace}/{name}` the user needs to
have been assigned either one of the set of `(admin or namespace-viewer or namespace-editor or namespace-owner)` or 
`(admin or namespace-editor or namespace-owner)` respectively.

The assignment of these roles follows the definitions set in the roles package. In short, a role is mapped
to IAM group(s). Following the example above, the default roles package defines these roles as:

```json
{
   "namespace-viewer": [
   "{root_group}/namespaces/{namespace}/viewer"
   ],
   "namespace-editor": [
   "{root_group}/namespaces/{namespace}/editor"
   ],
   "namespace-owner": [
   "{root_group}/namespaces/{namespace}/owner"
   ],
   "admin": [
   "{root_group}/roles/admin"
   ],
   ...
}
```

So that the required group membership to access the get metadata endpoint for the e.g. `testing` namespace would look
something like:

```
/services/data-management-api/
    testing/
        viewer||editor||owner
```

and likewise for the set metadata endpoint:

```
/services/data-management-api/
    testing/
        editor||owner
```

The `root_group` is a constant, set at application run-time. The `{namespace}` field is taken from the request path, 
e.g. if a user had requested `GET /metadata/test_namespace/test_name` the application would scan the user’s groups 
looking for membership of `{root_group}/test_namespace/admin||viewer||editor||owner`, this way, embargoes can be made on 
namespaces depending on IAM group membership.

For this authorisation path, the audience of the token is first checked against the expected audience from the policy.

### Token exchange (`type=exchange`)

For token exchange authorisation, the user’s groups are identified and membership is checked against the policy's 
`{root_group}/{iam_subgroup_name}` where `{root_group}` is a constant set at application run-time.

This route should be used before token exchanges are done at other APIs.

If authorisation is successful, the audience for the service (taken from the policy) is returned.

For this authorisation path, the audience of the token is not checked against any expected audience.

### Plugin (`type=plugin`)

## GMS

The Group Membership Service (GMS) is an IVOA service used to query IAM user group membership, returning a result that 
is compatible with other IVOA services. It is available at `/gms`.

## Development

Makefile targets have been included to facilitate easier and more consistent development against this API. The general 
recipe is as follows:

1. Depending on the fix type, create a new major/minor/patch branch, e.g. 
    ```bash
    $ make patch-branch NAME=some-name
    ```
    Note that this both creates and checkouts the branch.
2. Make your changes.
3. Add your changes to the branch:
    ```bash
   $ git add ...
    ```
4. Either commit the changes manually (if no version increment is needed) or bump the version and commit, entering a 
   commit message when prompted:
    ```bash
   $ make bump-and-commit
    ```
5. Push the changes upstream when ready:
    ```bash
   $ make push
    ```

Note that the CI pipeline will fail if python packages with the same semantic version are committed to the GitLab 
Package Registry.

## Deployment

Deployment is managed by docker-compose or helm.

The docker-compose file can be used to bring up the necessary services locally i.e. the REST API, setting the mandatory 
environment variables. Sensitive environment variables, including those relating to the IAM client, should be kept in 
`.env` files to avoid committing them to the repository.

There is also a helm chart for deployment onto a k8s cluster.

### Example via docker-compose

Edit the .env.template file accordingly and rename to .env, then:

```bash
eng@ubuntu:~/SKAO/ska-src-permissions-api$ docker-compose up
```

### Example via Helm

After creating a `values.yaml` (template in `/etc/helm/`):

```bash
$ create namespace ska-src-permissions-api
$ helm install --namespace ska-src-permissions-api ska-src-permissions-api .
```
