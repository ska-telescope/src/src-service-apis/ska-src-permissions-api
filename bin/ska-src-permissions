#!/usr/bin/env python

import argparse
import json

from ska_src_permissions_api.client.permissions import PermissionsClient


def add_args_for_authorise_by_route(authorise_parser_subparsers):
    authorise_by_route_parser = authorise_parser_subparsers.add_parser("route")
    authorise_by_route_parser.add_argument('-s', help="service", type=str)
    authorise_by_route_parser.add_argument('-v', help="version", type=str)
    authorise_by_route_parser.add_argument('-r', help="route", type=str)
    authorise_by_route_parser.add_argument('-m', help="method", type=str)
    authorise_by_route_parser.add_argument('-t', help="token", type=str)
    authorise_by_route_parser.add_argument('--body', help="extra arguments to be put in the request body in "
                                                          "comma-separated format <key>=<value>", type=str,
                                           default=None)


def add_args_for_authorise_by_service(authorise_parser_subparsers):
    authorise_by_service_parser = authorise_parser_subparsers.add_parser("service")
    authorise_by_service_parser.add_argument('-s', help="service", type=str)
    authorise_by_service_parser.add_argument('-v', help="version", type=str)
    authorise_by_service_parser.add_argument('-t', help="token", type=str)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--api-url', help="permissions api url", type=str,
                        default="https://permissions.srcdev.skao.int/api/v1")
    subparsers = parser.add_subparsers(help='command line interface for the permissions api', dest='command')

    # authorise
    authorise_parser = subparsers.add_parser("authorise")
    authorise_parser_subparsers = authorise_parser.add_subparsers(help="authorisation operations", dest='subcommand')

    ## by route
    add_args_for_authorise_by_route(authorise_parser_subparsers)

    ## by service
    add_args_for_authorise_by_service(authorise_parser_subparsers)

    # health
    health_parser = subparsers.add_parser("health")

    # ping
    ping_parser = subparsers.add_parser("ping")

    args = parser.parse_args()

    client = PermissionsClient(args.api_url)
    if args.command == 'authorise':
        if args.subcommand == 'route':
            body = {}
            if args.body is not None:
                for item in args.body.split(','):
                    key, value = item.split('=')
                    body[key] = value
            rtn = client.authorise_route_for_service(
                service=args.s, version=args.v, route=args.r, method=args.m, token=args.t, body=body)
        elif args.subcommand == 'service':
            rtn = client.authorise_service(service=args.s, version=args.v, token=args.t)
        else:
            print(authorise_parser.print_help())
    elif args.command == 'health':
        rtn = client.health()
        rtn.raise_for_status()
    elif args.command == 'ping':
        rtn = client.ping()
        rtn.raise_for_status()
    else:
        print(parser.print_help())
        exit()
    print(json.dumps(rtn.json(), indent=2))



